package main

import (
	"fmt"
	"log"

	"github.com/lukeroth/gdal"
)

func main() {
	// create a two-dimensional array -- 5x7 -- all 11s except the
	// point in the middle which is 31.
	inx := 5
	iny := 7
	inprod := inx * iny
	inarr := make([]int16, inprod)
	for i := 0; i < inprod; i++ {
		inarr[i] = 11
		if i == 17 {
			inarr[i] = 31
		}
		if i == 18 {
			inarr[i] = 32
		}
	}

	log.Print("inarr: ", inarr)

	// mem driver!
	memdrv, err1 := gdal.GetDriverByName("MEM")
	if err1 != nil {
		panic(err1)
	}

	// create a source band from that array
	srcDS := memdrv.Create("src", inx, iny, 1, gdal.Int16, nil)
	srcBand := srcDS.RasterBand(1)
	err2 := srcBand.IO(gdal.Write, 0, 0, inx, iny, inarr, inx, iny, 0, 0)
	if err2 != nil && err2.Error() != "No Error" {
		panic(err2)
	}

	// create a target band
	destDS := memdrv.Create("dest", inx, iny, 1, gdal.Int16, nil)
	destBand := destDS.RasterBand(1)

	// make a list of elements that are not 11
	dmap := make(map[int16]bool)
	for _, v := range inarr {
		if _, ok := dmap[v]; !ok {
			dmap[v] = true
		}
	}
	not11 := []string{}
	for k := range dmap {
		if k != 11 {
			not11 = append(not11, fmt.Sprintf("%d", k))
		}
	}
	log.Print("not11: ", not11)

	// configure options
	//	maxdepth := 2
	// options := []string{fmt.Sprintf("MAXDIST=%d", maxdepth), fmt.Sprintf("NODATA=%d", maxdepth), fmt.Sprintf("VALUES=%s", strings.Join(not11, ","))}
	// options := []string{"MAXDIST=2", "NODATA=2", "VALUES=31,32"}
	options := []string{""}

	// run computeproximity
	err3 := srcBand.ComputeProximity(destBand, options, gdal.DummyProgress, nil)
	if err3 != nil {
		panic(err3)
	}

	// get output
	outarr := make([]int16, inprod)
	err4 := destBand.IO(gdal.Read, 0, 0, inx, iny, outarr, inx, iny, 0, 0)
	if err4 != nil {
		panic(err4)
	}

	log.Print("outarr: ", outarr)
}
